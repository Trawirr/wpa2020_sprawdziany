import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import seaborn as sns
import sklearn.linear_model

# wczytaj dane;
df_og = pd.read_csv('concrete.csv')
df = pd.read_csv('concrete.csv')
pd.set_option('display.max_columns', None)

# wyznacz podstawowe statystyki opisowe poszczególnych zmiennych;
print("Podstawowe statystyki opisowe\n", df.describe())

# sprawdź czy w zbiorze występują braki danych;
# sprawdź czy dane są poprawne;
df.info()
'''nie ma żadnych braków, w każdej kolumnie jest 1030 poprawnych wartości
dane są poprawne, ponieważ w każdej kolumnie występują dane tylko jednego typu i nie ma wartości ujemnych, a równe 0 dopuszczalne'''

# sprawdź czy w zbiorze istnieją punkty oddalone, a jeżeli tak to je usuń;
# for column in df:
#     plt.figure()
#     df.boxplot([column], sym='k.')
#     plt.show()

'''na podstawie wykresów usuwam wartości odstające'''
mask = df['slag'] > 350
if not mask.empty:
    df.loc[mask, 'slag'] = np.nan
mask = df['water'] > 231
if not mask.empty:
    df.loc[mask, 'water'] = np.nan
mask = df['water'] < 125
if not mask.empty:
    df.loc[mask, 'water'] = np.nan
mask = df['superplastic'] > 25
if not mask.empty:
    df.loc[mask, 'superplastic'] = np.nan
mask = df['fineagg'] > 950
if not mask.empty:
    df.loc[mask, 'fineagg'] = np.nan

df = df.dropna()
df.info()
'''odstajacych wg wykresu age i strength nie usuwam'''

# narysuj wykresy wybranych zmiennych w zależności od zmiennej celu;
for column in df:
    plt.figure()
    df.boxplot([column], sym='k.')
    plt.show()
sns.pairplot(df, x_vars='strength', y_vars=df.iloc[:, :-1].columns)
plt.show()

# sprawdź jakie są korelacje pomiędzy zmiennymi;
corr = df.corr("pearson")
print('\nKorelacje:\n', corr)


# zbuduj model regresji liniowej na danych oryginalnych i sprawdź jego jakość za pomocą metryk R2, MAE i MSE;
def fit_regression(X_ucz, X_test, y_ucz, y_test, podzial=0.2):
    r = sklearn.linear_model.LinearRegression()
    r.fit(X_ucz, y_ucz)
    y_ucz_pred = r.predict(X_ucz)
    y_test_pred = r.predict(X_test)
    r2 = sklearn.metrics.r2_score
    mse = sklearn.metrics.mean_squared_error
    mae = sklearn.metrics.mean_absolute_error
    print("r_score_u:", r2(y_ucz, y_ucz_pred),
          "\nr_score_t:", r2(y_test, y_test_pred),
          "\nMSE_u:", mse(y_ucz, y_ucz_pred),
          "\nMSE_t:", mse(y_test, y_test_pred),
          "\nMAE_u:", mae(y_ucz, y_ucz_pred),
          "\nMAE_t:", mae(y_test, y_test_pred))
    return {
        "podzial": f'{int(100 - 100 * podzial)}:{int(100 * podzial)}',
        "r_score_u": r2(y_ucz, y_ucz_pred),
        "r_score_t": r2(y_test, y_test_pred),
        "MSE_u": mse(y_ucz, y_ucz_pred),
        "MSE_t": mse(y_test, y_test_pred),
        "MAE_u": mae(y_ucz, y_ucz_pred),
        "MAE_t": mae(y_test, y_test_pred)
    }


X_og = df_og.iloc[:, :-1]
y_og = df_og.iloc[:, -1]

X_ucz, X_test, y_ucz, y_test = sklearn.model_selection.train_test_split(X_og, y_og, test_size=0.2, random_state=12345)

print("\n---Model z danymi oryginalnymi---")
fit_regression(X_ucz, X_test, y_ucz, y_test)

# zbuduj model regresji liniowej na danych unormowanych i wystandaryzowanych oraz sprawdź jego jakość za pomocą metryk R2, MAE i MSE;
X = df.iloc[:, :-1]
X_std = (X - X.mean(axis=0)) / X.std(axis=0)
y = df.iloc[:, -1]
y_std = (y - y.mean()) / y.std()

X_ucz, X_test, y_ucz, y_test = sklearn.model_selection.train_test_split(X_std, y_std, test_size=0.2, random_state=12345)

print("\n---Model z danymi unormowanymi i wystandaryzowanymi---")
fit_regression(X_ucz, X_test, y_ucz, y_test)

# zbadaj jak zależy jakość modelu od wielkości podziału na zbiór treningowy i testowy (np. 90:10, 80:20, 70:30, ...);
wyniki = pd.DataFrame()
podzialy = [.1, .2, .3, .4, .5]
for podzial in podzialy:
    print(f"\n--- Podzial {int(100 - 100 * podzial)}:{int(100 * podzial)} ---")
    X_ucz, X_test, y_ucz, y_test = sklearn.model_selection.train_test_split(X, y, test_size=podzial, random_state=12345)
    res = fit_regression(X_ucz, X_test, y_ucz, y_test, podzial)
    wyniki = wyniki.append(res, ignore_index=True)

# przedstaw ostateczne wyniki i sformułuj wnioski;
'''
Wnioski: Najlepszą dokładność otrzymałem dla podziału 70:30. Obrobione dane zwiększają dokładność modelu, ale niewielką. Wyniki prezentują się następująco:
'''
print('\n--- Wyniki dla roznych podzialow ---\n', wyniki)
