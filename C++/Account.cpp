#pragma once
#include "Account.h"
#include "Bank.h"

Account::Account() :acc_number("123"), acc_cash(0.0) {}

Account::Account(string new_num, Client new_client, double new_cash):acc_number(new_num), acc_client(new_client), acc_cash(new_cash) {}

Account::Account(string new_num, Client new_client, double new_cash, string new_name, Address new_address) : acc_number(new_num), acc_client(new_client), acc_cash(new_cash) {
	name = new_name;
	address = new_address;
}


void Account::set_num(string new_num){
	acc_number = new_num;
}

void Account::set_client(Client new_client){
	acc_client = new_client;
}

void Account::set_cash(double new_cash){
	acc_cash = new_cash;
}

string Account::get_num(){
	return acc_number;
}

Client Account::get_client(){
	return acc_client;
}

double Account::get_cash(){
	return acc_cash;
}

void Account::withdraw(double cash){
	if (cash < 0) cout << "Niepoprawna kwota";
	else if (cash > acc_cash) cout << "Brak srodkow!" << endl;
	else acc_cash -= cash;
}

void Account::deposit(double cash) {
	if (cash < 0) cout << "Niepoprawna kwota";
	else acc_cash += cash;
}

void Account::disp(){
	cout << "Wlasciciel: " << acc_client.get_name() << " " << acc_client.get_lastname() << endl;
	cout << "Bank: " << name << " " << address.get_city() << ", " <<address.get_street()<<" "<< address.get_code()[0] << "-" << address.get_code()[1] << endl;
	cout << "Saldo: " << acc_cash << endl;
}
