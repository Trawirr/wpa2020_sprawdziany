#pragma once
#include "Address.h"
#include <iostream>
using namespace std;

Address::Address(){
	street = "ul. Bankowa";
	city = "Gdansk";
	num = 1;
	code[0] = 1;
	code[1] = 100;
}

Address::Address(string new_street, string new_city, int new_num, int * new_code) :street(new_street), city(new_city), num(new_num) {
	for (int i = 0; i < 2; i++) code[i] = new_code[i];
}

void Address::set_street(string new_street){
	street = new_street;
}

void Address::set_city(string new_city){
	city = new_city;
}

void Address::set_num(int new_num){
	num = new_num;
}

void Address::set_code(int *new_code){
	for (int i = 0; i < 2; i++) code[i] = new_code[i];
}

string Address::get_street(){
	return street;
}

string Address::get_city(){
	return city;
}

int Address::get_num(){
	return num;
}

int * Address::get_code()
{
	return code;
}

void Address::disp(){
	cout << "Adres: " << city << ", " << street << " " << code[0] << "-" << code[1] << endl;
}

