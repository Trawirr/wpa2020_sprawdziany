#pragma once

#include "Bank.h"
#include "Client.h"

class Account : public Bank {
	string acc_number;
	Client acc_client;
	double acc_cash;
public:
	Account();
	Account(string, Client, double);
	Account(string, Client, double, string, Address);

	void set_num(string);
	void set_client(Client);
	void set_cash(double);

	string get_num();
	Client get_client();
	double get_cash();

	void withdraw(double);
	void deposit(double);

	void disp();
};