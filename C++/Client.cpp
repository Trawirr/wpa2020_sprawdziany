#pragma once

#include <iostream>
#include "Client.h"

using namespace std;

Client::Client():name("Marcin"), lastname("Garnowski"){
	Address new_address;
	address = new_address;
}

Client::Client(string new_name, string new_lastname, Address new_address):name(new_name), lastname(new_lastname), address(new_address){}

void Client::set_name(string new_name){
	name = new_name;
}

void Client::set_lastname(string new_lastname){
	lastname = new_lastname;
}

void Client::set_address(Address new_address){
	address = new_address;
}

string Client::get_name(){
	return name;
}

string Client::get_lastname(){
	return lastname;
}

Address Client::get_address(){
	return address;
}

void Client::disp(){
	cout << "Imie i nazwisko: " << name << " " << lastname << endl;
	cout<<"Adres: " << address.get_city() << ", " << address.get_street() << " " << address.get_code()[0] << "-" << address.get_code()[1] << endl;
}
