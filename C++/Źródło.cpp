#include <iostream>
#include "Address.h"
#include "Client.h"
#include "Bank.h"
#include "Account.h"
using namespace std;

int main() {

	Address my_address;
	Client my_client;
	Bank my_bank;

	my_address.disp();
	cout << endl;
	my_client.disp();
	cout << endl;
	my_bank.disp();
	cout << endl;

	Account my_acc;
	cout << endl << "Symulacja rachunku bankowego:" << endl;
	cout << endl << "------------1------------" << endl;
	my_acc.disp();
	cout << endl << "------------2------------" << endl;
	cout << "Wplacenie 150 na rachunek" << endl;
	my_acc.deposit(150.0);
	my_acc.disp();
	cout << endl << "------------3------------" << endl;
	cout << "Wyplacenie 95.90 z rachunku" << endl;
	my_acc.withdraw(95.90);
	my_acc.disp();
	cout << endl << "------------4------------" << endl;
	cout << "Proba wyplacenia 100 z rachunku" << endl;
	my_acc.withdraw(100.0);
	my_acc.disp();

	cout << endl;
	system("pause");
	return 0;
}