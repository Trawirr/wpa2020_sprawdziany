#pragma once

#include "Bank.h"

Bank::Bank():name("TypicalBank"){}

Bank::Bank(string new_name, Address new_address):name(new_name), address(new_address){}

void Bank::set_name(string new_name){
	name = new_name;
}

void Bank::set_address(Address new_address){
	address = new_address;
}

string Bank::get_name(){
	return name;
}

Address Bank::get_address(){
	return address;
}

void Bank::disp(){
	cout << "Nazwa: " << name << endl;
	cout << "Adres: " << address.get_city() << ", " << address.get_street() << " " << address.get_code()[0] << "-" << address.get_code()[1] << endl;
}
