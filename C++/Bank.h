#pragma once

#include <iostream>
#include <string>
#include "Address.h"

class Bank {
protected:
	string name;
	Address address;
public:
	Bank();
	Bank(string, Address);

	void set_name(string);
	void set_address(Address);

	string get_name();
	Address get_address();

	void disp();
};