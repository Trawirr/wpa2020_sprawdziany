#pragma once

#include <iostream>
#include <string>

using namespace std;

class Address {
	string street;
	string city;
	int num;
	int code[2];
public:
	Address();
	Address(string, string, int, int*);

	void set_street(string);
	void set_city(string);
	void set_num(int);
	void set_code(int*);

	string get_street();
	string get_city();
	int get_num();
	int* get_code();

	void disp();

};