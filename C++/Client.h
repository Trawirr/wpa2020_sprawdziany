#pragma once
#include <string>
#include "Address.h"

class Client {
	string name;
	string lastname;
	Address address;
public:
	Client();
	Client(string, string, Address);

	void set_name(string);
	void set_lastname(string);
	void set_address(Address);

	string get_name();
	string get_lastname();
	Address get_address();

	void disp();
};