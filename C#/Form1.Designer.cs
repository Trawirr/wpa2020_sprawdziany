﻿namespace Quiz_wpa
{
    partial class Form1
    {
        /// <summary>
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod generowany przez Projektanta formularzy systemu Windows

        /// <summary>
        /// Metoda wymagana do obsługi projektanta — nie należy modyfikować
        /// jej zawartości w edytorze kodu.
        /// </summary>
        private void InitializeComponent()
        {
            this.label_question1 = new System.Windows.Forms.Label();
            this.label_question2 = new System.Windows.Forms.Label();
            this.label_question3 = new System.Windows.Forms.Label();
            this.label_question4 = new System.Windows.Forms.Label();
            this.label_question5 = new System.Windows.Forms.Label();
            this.comboBox_question1 = new System.Windows.Forms.ComboBox();
            this.label_score_text = new System.Windows.Forms.Label();
            this.button_submit = new System.Windows.Forms.Button();
            this.textBox_question2 = new System.Windows.Forms.TextBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.dateTimePicker_question4 = new System.Windows.Forms.DateTimePicker();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.checkBox3 = new System.Windows.Forms.CheckBox();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            this.label_score = new System.Windows.Forms.Label();
            this.button_reset = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label_question1
            // 
            this.label_question1.AutoSize = true;
            this.label_question1.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.25F);
            this.label_question1.Location = new System.Drawing.Point(12, 9);
            this.label_question1.Name = "label_question1";
            this.label_question1.Size = new System.Drawing.Size(495, 22);
            this.label_question1.TabIndex = 0;
            this.label_question1.Text = "1. Który YouTuber został nominowany do #hot16challenge2?";
            // 
            // label_question2
            // 
            this.label_question2.AutoSize = true;
            this.label_question2.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.25F);
            this.label_question2.Location = new System.Drawing.Point(12, 109);
            this.label_question2.Name = "label_question2";
            this.label_question2.Size = new System.Drawing.Size(276, 22);
            this.label_question2.TabIndex = 1;
            this.label_question2.Text = "2. Który raper rozpoczął tę akcję?";
            // 
            // label_question3
            // 
            this.label_question3.AutoSize = true;
            this.label_question3.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.25F);
            this.label_question3.Location = new System.Drawing.Point(12, 209);
            this.label_question3.Name = "label_question3";
            this.label_question3.Size = new System.Drawing.Size(340, 22);
            this.label_question3.TabIndex = 2;
            this.label_question3.Text = "3. Przez kogo został nominowany Sarius?";
            // 
            // label_question4
            // 
            this.label_question4.AutoSize = true;
            this.label_question4.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.25F);
            this.label_question4.Location = new System.Drawing.Point(12, 339);
            this.label_question4.Name = "label_question4";
            this.label_question4.Size = new System.Drawing.Size(573, 22);
            this.label_question4.TabIndex = 3;
            this.label_question4.Text = "4. Którego dnia został opublikowany pierwszy filmik #hot16challenge2?";
            // 
            // label_question5
            // 
            this.label_question5.AutoSize = true;
            this.label_question5.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.25F);
            this.label_question5.Location = new System.Drawing.Point(12, 439);
            this.label_question5.Name = "label_question5";
            this.label_question5.Size = new System.Drawing.Size(335, 22);
            this.label_question5.TabIndex = 4;
            this.label_question5.Text = "5. Na jaki cel raperzy zbierają pieniądze?";
            // 
            // comboBox_question1
            // 
            this.comboBox_question1.FormattingEnabled = true;
            this.comboBox_question1.Items.AddRange(new object[] {
            "Krzysztof Gonciarz",
            "Gargamel",
            "Klocuch",
            "Stuu",
            "Generator Frajdy"});
            this.comboBox_question1.Location = new System.Drawing.Point(15, 50);
            this.comboBox_question1.Name = "comboBox_question1";
            this.comboBox_question1.Size = new System.Drawing.Size(153, 21);
            this.comboBox_question1.TabIndex = 5;
            // 
            // label_score_text
            // 
            this.label_score_text.AutoSize = true;
            this.label_score_text.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F);
            this.label_score_text.Location = new System.Drawing.Point(675, 85);
            this.label_score_text.Name = "label_score_text";
            this.label_score_text.Size = new System.Drawing.Size(96, 31);
            this.label_score_text.TabIndex = 6;
            this.label_score_text.Text = "Wynik:";
            // 
            // button_submit
            // 
            this.button_submit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.button_submit.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.25F);
            this.button_submit.Location = new System.Drawing.Point(656, 405);
            this.button_submit.Name = "button_submit";
            this.button_submit.Size = new System.Drawing.Size(150, 90);
            this.button_submit.TabIndex = 7;
            this.button_submit.Text = "Zatwierdź";
            this.button_submit.UseVisualStyleBackColor = false;
            this.button_submit.Click += new System.EventHandler(this.button_submit_Click);
            // 
            // textBox_question2
            // 
            this.textBox_question2.Location = new System.Drawing.Point(12, 153);
            this.textBox_question2.Name = "textBox_question2";
            this.textBox_question2.Size = new System.Drawing.Size(156, 20);
            this.textBox_question2.TabIndex = 8;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(12, 249);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(56, 17);
            this.checkBox1.TabIndex = 9;
            this.checkBox1.Text = "Filipek";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // dateTimePicker_question4
            // 
            this.dateTimePicker_question4.Location = new System.Drawing.Point(12, 390);
            this.dateTimePicker_question4.Name = "dateTimePicker_question4";
            this.dateTimePicker_question4.Size = new System.Drawing.Size(200, 20);
            this.dateTimePicker_question4.TabIndex = 10;
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Location = new System.Drawing.Point(12, 478);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(156, 17);
            this.radioButton1.TabIndex = 11;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "Ochrona Polski przed suszą";
            this.radioButton1.UseVisualStyleBackColor = true;
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Location = new System.Drawing.Point(12, 272);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(59, 17);
            this.checkBox2.TabIndex = 12;
            this.checkBox2.Text = "Paluch";
            this.checkBox2.UseVisualStyleBackColor = true;
            // 
            // checkBox3
            // 
            this.checkBox3.AutoSize = true;
            this.checkBox3.Location = new System.Drawing.Point(12, 295);
            this.checkBox3.Name = "checkBox3";
            this.checkBox3.Size = new System.Drawing.Size(51, 17);
            this.checkBox3.TabIndex = 13;
            this.checkBox3.Text = "Gedz";
            this.checkBox3.UseVisualStyleBackColor = true;
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(12, 501);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(150, 17);
            this.radioButton2.TabIndex = 14;
            this.radioButton2.TabStop = true;
            this.radioButton2.Text = "Muzyczna edukacja dzieci";
            this.radioButton2.UseVisualStyleBackColor = true;
            // 
            // radioButton3
            // 
            this.radioButton3.AutoSize = true;
            this.radioButton3.Location = new System.Drawing.Point(12, 524);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new System.Drawing.Size(185, 17);
            this.radioButton3.TabIndex = 15;
            this.radioButton3.TabStop = true;
            this.radioButton3.Text = "Pomoc lekarzom z koronawirusem";
            this.radioButton3.UseVisualStyleBackColor = true;
            // 
            // label_score
            // 
            this.label_score.AutoSize = true;
            this.label_score.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F);
            this.label_score.Location = new System.Drawing.Point(693, 142);
            this.label_score.Name = "label_score";
            this.label_score.Size = new System.Drawing.Size(56, 31);
            this.label_score.TabIndex = 16;
            this.label_score.Text = "      ";
            this.label_score.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // button_reset
            // 
            this.button_reset.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.button_reset.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F);
            this.button_reset.Location = new System.Drawing.Point(656, 517);
            this.button_reset.Name = "button_reset";
            this.button_reset.Size = new System.Drawing.Size(150, 30);
            this.button_reset.TabIndex = 17;
            this.button_reset.Text = "Reset";
            this.button_reset.UseVisualStyleBackColor = false;
            this.button_reset.Click += new System.EventHandler(this.button_reset_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Silver;
            this.ClientSize = new System.Drawing.Size(884, 561);
            this.Controls.Add(this.button_reset);
            this.Controls.Add(this.label_score);
            this.Controls.Add(this.radioButton3);
            this.Controls.Add(this.radioButton2);
            this.Controls.Add(this.checkBox3);
            this.Controls.Add(this.checkBox2);
            this.Controls.Add(this.radioButton1);
            this.Controls.Add(this.dateTimePicker_question4);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.textBox_question2);
            this.Controls.Add(this.button_submit);
            this.Controls.Add(this.label_score_text);
            this.Controls.Add(this.comboBox_question1);
            this.Controls.Add(this.label_question5);
            this.Controls.Add(this.label_question4);
            this.Controls.Add(this.label_question3);
            this.Controls.Add(this.label_question2);
            this.Controls.Add(this.label_question1);
            this.Name = "Form1";
            this.Text = "Quiz";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label_question1;
        private System.Windows.Forms.Label label_question2;
        private System.Windows.Forms.Label label_question3;
        private System.Windows.Forms.Label label_question4;
        private System.Windows.Forms.Label label_question5;
        private System.Windows.Forms.ComboBox comboBox_question1;
        private System.Windows.Forms.Label label_score_text;
        private System.Windows.Forms.Button button_submit;
        private System.Windows.Forms.TextBox textBox_question2;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.DateTimePicker dateTimePicker_question4;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.CheckBox checkBox3;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton3;
        private System.Windows.Forms.Label label_score;
        private System.Windows.Forms.Button button_reset;
    }
}

