﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Quiz_wpa
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button_submit_Click(object sender, EventArgs e)
        {
            int score = 0;

            // pytanie 1 - Klocuch został nominowany przez Taco
            if (comboBox_question1.Text == "Klocuch")
            {
                label_question1.ForeColor = Color.Green;
                score++;
            }
            else label_question1.ForeColor = Color.Red;

            // pytanie 2 - Solar rozpoczął dwukrotnie
            if (textBox_question2.Text == "Solar")
            {
                label_question2.ForeColor = Color.Green;
                score++;
            }
            else label_question2.ForeColor = Color.Red;

            // pytanie 3 - Wszyscy trzej nominowali Sariusa
            if (checkBox1.Checked && checkBox2.Checked && checkBox3.Checked)
            {
                label_question3.ForeColor = Color.Green;
                score++;
            }
            else label_question3.ForeColor = Color.Red;

            // pytanie 4 - Solar opublikował filmik 28 kwietnia 2020
            // nie wiem, czy przy VS w innym jezyku nie będzie innego formatu, u mnie działa :D
            if (dateTimePicker_question4.Value.ToShortDateString() == "28.04.2020")
            {
                label_question4.ForeColor = Color.Green;
                score++;
            }
            else label_question4.ForeColor = Color.Red;

            // pytanie 5 - zbierają na wsparcie dla lekarzy
            if (radioButton3.Checked)
            {
                label_question5.ForeColor = Color.Green;
                score++;
            }
            else label_question5.ForeColor = Color.Red;
            
            label_score.Text = score + "/5";
            string msg;
            if(score == 5)
            {
                label_score.ForeColor = Color.Green;
                msg = "Udało Ci się zdobyć wszystkie " + score + "/5 punktów.\nLepiej się nie dało, gratulacje!";
            }
            else if (score >= 3)
            {
                label_score.ForeColor = Color.Green;
                msg = "Udało Ci się zdobyć " + score + "/5 punktów.\nGratulacje!";
            }
            else
            {
                label_score.ForeColor = Color.Red;
                msg = "Twój wynik to " + score + "/5 punktów.\nSpróbuj rozwiązać to lepiej!";
            }
            MessageBox.Show(msg, "Wynik", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void button_reset_Click(object sender, EventArgs e)
        {
            comboBox_question1.Text = "";
            textBox_question2.Text = "";
            checkBox1.Checked = false;
            checkBox2.Checked = false;
            checkBox3.Checked = false;
            dateTimePicker_question4.Value = DateTime.Now;
            radioButton1.Checked = false;
            radioButton2.Checked = false;
            radioButton3.Checked = false;
            label_score.Text = "";
            label_question1.ForeColor = Color.Black;
            label_question2.ForeColor = Color.Black;
            label_question3.ForeColor = Color.Black;
            label_question4.ForeColor = Color.Black;
            label_question5.ForeColor = Color.Black;
        }
    }
}
